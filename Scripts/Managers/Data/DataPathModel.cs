﻿using System.Linq;
using System.Security.Cryptography;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "DataPath", menuName = "Path System/Path Model", order = 1)]
public class DataPathModel : ScriptableObject {
    
    public enum NodeStatus {
        Inactive,
        Active,
        Success,
        Failed
    }

    [System.Serializable]
    public class DataPathNode {
        public string title;
        public string id;
        public NodeStatus status;

        public Vector2 savedPosition;

        public DataPathNode (string title, NodeStatus status, int idCount = 0, Vector2 savedPosition = default) {
            this.title = title;
            using (SHA1Managed sha = new SHA1Managed()) {
                id = string.Concat (sha.ComputeHash (System.Text.Encoding.UTF8.GetBytes (title)).Select (b => b.ToString ("X2")).Take(5));
                id = id.Insert (4, "-");
            }
            this.status = status;
            this.savedPosition = savedPosition;
        }
    }

    public DataPathNode[] pathNodes;

    #region Asset Creation
    public DataPathModel () {
        pathNodes = new DataPathNode[1];
        pathNodes[0] = new DataPathNode ("Start Objective", NodeStatus.Active);
    }
    private static DataPathModel InitFromArray (DataPathNode[] pathNodeArray) {
        DataPathModel model = CreateInstance<DataPathModel> ();
        model.pathNodes = new DataPathNode[pathNodeArray.Length];
        pathNodeArray.CopyTo (model.pathNodes, 0);
        return model;
    }
    public static DataPathModel CreateAssetInstance (DataPathNode[] pathNodeArray) {
        AssetDatabase.CreateAsset (InitFromArray (pathNodeArray), DataPathManager.EDT_DIR);
        AssetDatabase.SaveAssets ();
        return (DataPathModel)AssetDatabase.LoadAssetAtPath (DataPathManager.EDT_DIR, typeof (DataPathModel));
    }
    public static DataPathModel CreateAssetInstance (DataPathModel model) {
        AssetDatabase.CreateAsset (model, DataPathManager.EDT_DIR);
        AssetDatabase.SaveAssets ();
        return (DataPathModel)AssetDatabase.LoadAssetAtPath (DataPathManager.EDT_DIR, typeof (DataPathModel));
    }
    #endregion
}
