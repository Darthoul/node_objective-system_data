﻿using System;
using UnityEngine;

public class NodeContent {

    NodeContentSegment title;
    NodeContentSegment id;
    NodeContentSegment status;
    public NodeContentSegment[] contentCollection;

    float rectOffsetX = 44f;
    float rectOffsetY = 12f;

    public string titleLabel { get { return title.label; } set { title.label = value; } }
    public DataPathModel.NodeStatus statusData { get { return (DataPathModel.NodeStatus)Enum.Parse (typeof (DataPathModel.NodeStatus), status.label); } }

    public class NodeContentSegment {
        public string label;
        public Rect rectBox;
        public GUIStyle style;
        public string header;

        public NodeContentSegment (string label, Rect rectBox = default, GUIStyle style = null, string header = null) {
            this.label = label;
            this.rectBox = (rectBox == default) ? new Rect (Vector2.zero, Vector2.up * 20f) : rectBox;
            this.style = (style == null) ? GUIStyle.none : style;
            this.header = (header == null) ? string.Empty : header;
        }
    }

    public NodeContent (string titleName = "Dummy Title", string idName = "0000-000000", string statusName = "Inactive") {
        title = new NodeContentSegment (titleName, style: "LODRendererRemove");
        id = new NodeContentSegment (idName, header: "ID: ");
        status = new NodeContentSegment (statusName, style: "ChannelStripAttenuationMarkerSquare");
        contentCollection = new NodeContentSegment[3];
        contentCollection[0] = title;
        contentCollection[1] = id;
        contentCollection[2] = status;
        Debug.Log (title);
    }

    public Rect GetContentSizeRect (Rect source) {
        source.height -= 10f;
        foreach (NodeContentSegment segment in contentCollection) {
            source.height += segment.rectBox.height;
        }
        return source;
    }

    public void Draw (Rect nodeRect) {
        for (int i = 0; i < contentCollection.Length; i++) {
            contentCollection[i].rectBox = SetContentRect (nodeRect, contentCollection[i].rectBox.height);
            GUI.Label (contentCollection[i].rectBox, contentCollection[i].header + contentCollection[i].label, contentCollection[i].style);

            nodeRect.y += contentCollection[i].rectBox.height;
        }
    }

    Rect SetContentRect (Rect source, float height) {
        Rect targetRect = new Rect ();
        targetRect.x = source.x + (rectOffsetX / 2);
        targetRect.y = source.y + rectOffsetY;
        targetRect.width = source.width - rectOffsetX;
        targetRect.height = height;

        return targetRect;
    }
}
