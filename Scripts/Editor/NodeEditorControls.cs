﻿using System;
using UnityEngine;

public class NodeEditorControls {

    public const float P_WIDTH = 200f;

    EditorControlsButton clear;
    EditorControlsButton save;
    EditorControlsButton load;

    EditorControlsButton[] buttonCollection;

    static readonly Vector2 padding = new Vector2 (4f, 4f);
    static readonly Color neutralColor = new Color (0.28f, 0.3f, 0.28f);

    GUIStyle noticeStyle;
    GameObject node;

    public class EditorControlsButton {
        public string label;
        public Rect rectBox;
        public Action action;

        public static readonly Rect defaultButtonSize = new Rect (0, 0, 50, 15);

        public EditorControlsButton (string label, Rect rectBox = default, Action action = null) {
            this.label = label;
            this.rectBox = (rectBox == default) ? defaultButtonSize : rectBox;
            this.action = action;
        }
    }

    public void Draw (Rect windowPosition, Node selection) {
        Rect panelRect = new Rect (windowPosition.width - P_WIDTH, 0, P_WIDTH, windowPosition.height);
        GUI.Box (panelRect, string.Empty, "CurveEditorBackground");
        DrawButtons (panelRect);
        DrawNodeInspector (panelRect, selection);
    }

    public void DrawButtons (Rect currentPanelRect) {
        foreach (EditorControlsButton button in buttonCollection) {
            if (GUI.Button (button.rectBox, button.label)) {
                button.action?.Invoke ();
            }
        }
        float spacing = EditorControlsButton.defaultButtonSize.width + padding.x;
        Vector2 offset = new Vector2 (16f, 8f);
        for (int i = 0; i < buttonCollection.Length; i++) {
            Vector2 currentSpacing = new Vector2 (spacing * i, 0);
            buttonCollection[i].rectBox.position = offset + currentPanelRect.position + padding + currentSpacing;
        }
    }

    public void DrawNodeInspector (Rect currentPanelRect, Node selection) {
        Vector2 offset = new Vector2 (10f, 0);
        currentPanelRect.position += padding + offset;
        currentPanelRect.size -= (padding + offset) * 2;
        currentPanelRect.y += EditorControlsButton.defaultButtonSize.height + padding.y + 20f;
        currentPanelRect.height = 40f;
        GUI.Box (currentPanelRect, string.Empty, UnityEditor.EditorStyles.helpBox);
        GUI.Label (currentPanelRect, "Inspector", "dockarea");
        Rect insElementRect = GetElementRect (currentPanelRect, 16f);
        if (selection != null) {
            selection.content.titleLabel = GUI.TextArea (insElementRect, selection.content.titleLabel);
        } else {
            GUI.Label (insElementRect, " Nothing currently selected", noticeStyle);
        }
    }

    private Rect GetElementRect (Rect source, float spacing) {
        source.y += spacing;
        source.height = spacing;
        source.width -= padding.y * 2;
        source.position += padding;
        return source;
    }

    public NodeEditorControls () {
        clear = new EditorControlsButton ("Clear", action: NodeBasedEditor.ClearNodes);
        save = new EditorControlsButton ("Save", action: NodeBasedEditor.SaveNodes);
        load = new EditorControlsButton ("Load", action: NodeBasedEditor.LoadNodes);
        buttonCollection = new EditorControlsButton[3];
        buttonCollection[0] = clear;
        buttonCollection[1] = save;
        buttonCollection[2] = load;

        noticeStyle = new GUIStyle ();
        noticeStyle.fontStyle = FontStyle.Italic;
        noticeStyle.fontSize = 10;
        noticeStyle.normal.textColor = neutralColor;
    }
}
