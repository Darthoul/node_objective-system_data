# Node Objective-System data

Creating a system for visually configuration of missions/objectives using Gram's NodeBasedEditor as a base and ScriptableObjects

- The base code for the project is available at https://gram.gs/gramlog/creating-node-based-editor-unity/ (Gram.gs)

- BuiltInResourcesWindow.cs from daemon3000 (https://gist.github.com/daemon3000/ed9e617330e8b095bbf8) makes a tool to read the various GUIStyles and visualize them on a window. (Highly recommended for custom editor windows)

Currently working on the node content:
* NodeContent.cs <- Manages the content of the Node and contains info about the sizing of Nodes
* NodeEditorControls <- Manages the inspector panel for the Nodes

